(("default" . ((user-emacs-directory . "~/.config/badymacs")))
 ("debug" . ((user-emacs-directory . "/tmp/emacs.d")))
 ("badymacs" . ((user-emacs-directory . "~/.config/badymacs")))
 ("spacemacs" . ((user-emacs-directory . "~/.config/spacemacs")))
 ("doom" . ((user-emacs-directory . "~/.local/src/doom-emacs")
	    (env . (("DOOMDIR" . "~/.config/doom"))))))
 
