#!/bin/bash
# Notify when ram usage go above 90%
# Forked from the script at:
# https://bbs.archlinux.org/viewtopic.php?pid=1037160#p1037160
# RAM monitor reference: https://stackoverflow.com/a/10586020

SLEEP_TIME=300   # Default time between checks.
SAFE_PERCENT=80  # Still safe at this level.
DANGER_PERCENT=90  # Warn when RAM at this level.
CRITICAL_PERCENT=95  # Suspend when RAM at this level.

export DISPLAY=:0.0

function sendNoti
{
    notify-send -u critical "Critical RAM Usage" "Beware of memory hungry processes!"
}

while [ true ]; do
    ram_usage=$(free | awk '/Mem/{printf("%d\n"), $3/$2*100}')

    if [[ $ram_usage -lt $SAFE_PERCENT ]]; then
        SLEEP_TIME=300
    else
        SLEEP_TIME=120
        if [[ $ram_usage -ge $DANGER_PERCENT ]]; then
            SLEEP_TIME=60
            sendNoti
        fi
        if [[ $ram_usage -ge $CRITICAL_PERCENT ]]; then
            SLEEP_TIME=30
            systemctl suspend
        fi
    fi

    sleep ${SLEEP_TIME}

done
