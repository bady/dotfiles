#!/bin/bash
# Forked from the script at:
# https://bbs.archlinux.org/viewtopic.php?pid=1037160#p1037160

SLEEP_TIME=5   # Default time between checks.
SAFE_PERCENT=30  # Still safe at this level.
DANGER_PERCENT=15  # Warn when battery at this level.
CRITICAL_PERCENT=5  # Suspend when battery at this level.

export DISPLAY=:0.0

function sendNoti
{
    notify-send -u critical "Low battery" "Better charge now!"
}


while [ true ]; do

    if [[ -n $(acpi -b | grep -i discharging) ]]; then
        rem_bat=$(acpi -b | head -1 | grep -Eo "[0-9]+%" | grep -Eo "[0-9]+")

        if [[ $rem_bat -gt $SAFE_PERCENT ]]; then
            SLEEP_TIME=10
        else
            SLEEP_TIME=5
            if [[ $rem_bat -le $DANGER_PERCENT ]]; then
                SLEEP_TIME=2
                sendNoti
            fi
            if [[ $rem_bat -le $CRITICAL_PERCENT ]]; then
                SLEEP_TIME=1
                systemctl suspend
            fi
        fi
    else
        SLEEP_TIME=10
    fi

    sleep ${SLEEP_TIME}m

done
